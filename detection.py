import cv2 as cv
import mediapipe as mp
import time
from subprocess import call


class HandDetection:
    def __init__(self, static_image_mode=False, max_num_hands=2, min_detection_confidence=0.5,
                 min_tracking_confidence=0.5):
        self.mp_hands = mp.solutions.hands
        self.hands = self.mp_hands.Hands(static_image_mode=static_image_mode,
                                         max_num_hands=max_num_hands,
                                         min_detection_confidence=min_detection_confidence,
                                         min_tracking_confidence=min_tracking_confidence)
        self.mp_draw = mp.solutions.drawing_utils

    def find_hands(self, img, draw=True):
        rgb = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        self.results = self.hands.process(rgb)
        if self.results.multi_hand_landmarks:
            for hand in self.results.multi_hand_landmarks:
                for id, lm in enumerate(hand.landmark):
                    print(id, lm)
                    height, width, dim = img.shape
                    coord_x, coord_y = int(lm.x * width), int(lm.y * height)
                    print(id, (coord_x, coord_y))
                    if draw:
                        self.mp_draw.draw_landmarks(img, hand, self.mp_hands.HAND_CONNECTIONS)
        return img

    def find_position(self, img, hand_number=0, draw=True) -> list:
        landmark_list = []
        if self.results.multi_hand_landmarks:
            points = self.results.multi_hand_landmarks[hand_number]
            for id, lm in enumerate(points.landmark):
                height, width, dim = img.shape
                coord_x, coord_y = int(lm.x * width), int(lm.y * height)
                landmark_list.append([id, coord_x, coord_y])
                if draw:
                    cv.circle(img, (coord_x, coord_y), 25, (255, 0, 0), -1)
        return landmark_list


def change_volume(volume):
    call(["osascript", "-e", f"set Volume {volume}"])


def run():
    hand_detection = HandDetection(max_num_hands=4)
    current_time, past_time = 0, 0

    capture = cv.VideoCapture(0)
    while True:
        success, img = capture.read()

        img = hand_detection.find_hands(img, draw=True)
        lm_list = hand_detection.find_position(img, 0)
        if len(lm_list) > 0:
            print(lm_list[8])
        current_time = time.time()
        fps = 1 / (current_time - past_time)
        past_time = current_time

        cv.putText(img, str(int(fps)), (10, 70), cv.FONT_HERSHEY_PLAIN, 3, (255, 0, 0), 3)

        cv.imshow('Video Streaming', img)
        cv.waitKey(1)


if __name__ == '__main__':
    run()
