import cv2 as cv
import time
import detection
import math
import numpy as np

camera_width = 1440
camera_height = 720
previous_time = 0
min_finger_distance = 50
max_finger_distance = 400
min_volume = 0
max_volume = 10

capture = cv.VideoCapture(0)

capture.set(3, camera_width)
capture.set(4, camera_height)

detector = detection.HandDetection(min_detection_confidence=0.7)

while True:
    success, img = capture.read()
    current_time = time.time()
    fps = str(int((1 / (current_time - previous_time))))
    previous_time = current_time

    img = detector.find_hands(img)
    fingers_coord = detector.find_position(img, draw=False)

    if len(fingers_coord) > 0:
        thumb_coord = fingers_coord[4]
        index_coord = fingers_coord[8]
        thumb_coord_x, thumb_coord_y = thumb_coord[1], thumb_coord[2]
        index_coord_x, index_coord_y = index_coord[1], index_coord[2]
        centre_distance_x, centre_distance_y = (thumb_coord_x + index_coord_x) // 2, (
                thumb_coord_y + index_coord_y) // 2
        cv.circle(img, (index_coord_x, index_coord_y), 15, (255, 0, 0), -1)
        cv.circle(img, (thumb_coord_x, thumb_coord_y), 15, (255, 0, 0), -1)
        cv.line(img, (thumb_coord_x, thumb_coord_y), (index_coord_x, index_coord_y), (30, 155, 55), 3)
        cv.circle(img, (centre_distance_x, centre_distance_y), 15, (255, 0, 0), -1)
        finger_length = math.hypot(thumb_coord_x - index_coord_x, thumb_coord_y - index_coord_y)
        wish_volume = np.interp(finger_length, [min_finger_distance, max_finger_distance], [min_volume, max_volume])
        print(finger_length, wish_volume)
        detection.change_volume(wish_volume)
        cv.putText(img, f'Volume: {wish_volume}', (10, 170), cv.FONT_HERSHEY_PLAIN, 3, (255, 0, 0), 3)

        if finger_length < min_finger_distance:
            cv.circle(img, (centre_distance_x, centre_distance_y), 15, (0, 255, 0), -1)

    # cv.putText(img, f'FPS: {fps}', (10, 70), cv.FONT_HERSHEY_PLAIN, 3, (255, 0, 0), 3)
    cv.imshow('Hand tracking', img)
    cv.waitKey(1)
